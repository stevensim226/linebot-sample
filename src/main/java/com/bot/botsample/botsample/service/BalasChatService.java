package com.bot.botsample.botsample.service;

import java.net.URI;

public interface BalasChatService {

    public String balasChat(String receivedMessage);

    public String balasUserId(String userId);

    public URI balasImage();

    public String balasPush();
}
