package com.bot.botsample.botsample.service;

import org.springframework.stereotype.Service;

import java.net.URI;

@Service
public class BalasChatServiceImpl implements BalasChatService {
    @Override
    public String balasChat(String receivedMessage) {
        return "Hello World! \n anda mengirimkan: " + receivedMessage;
    }

    @Override
    public String balasUserId(String userId) {
        return "User ID anda adalah " + userId;
    }

    @Override
    public URI balasImage() {
        return URI.create("https://i.imgur.com/PVQct4B.png");
    }

    @Override
    public String balasPush() {
        return "Push message akan dikirimkan dalam beberapa saat.";
    }
}
