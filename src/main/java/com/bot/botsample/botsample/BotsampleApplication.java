package com.bot.botsample.botsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotsampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotsampleApplication.class, args);
	}

}
