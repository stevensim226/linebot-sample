package com.bot.botsample.botsample.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @GetMapping(path = "/ping")
    public ResponseEntity getPing() {
        return ResponseEntity.ok("pong");
    }
}
