package com.bot.botsample.botsample.controller;

import com.bot.botsample.botsample.service.BalasChatService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@LineMessageHandler
public class LineController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    BalasChatService balasChatService;

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent){
        String pesan = messageEvent.getMessage().getText().toLowerCase();
        String userId = messageEvent.getSource().getSenderId();
        String senderId = messageEvent.getSource().getSenderId();

        String replyToken = messageEvent.getReplyToken();

//        List<Message> msgList = new ArrayList<Message>();
//        msgList.add(jawabanDalamBentukTextMessage);
//        msgList.add(imageMessage);

        Message newMsg;
        switch (pesan.toUpperCase()) {
            case "GET ID":
                newMsg = new TextMessage(balasChatService.balasUserId(userId));
                break;
            case "SAY HELLO":
                newMsg = new TextMessage(balasChatService.balasChat(pesan));
                break;
            case "SEND IMAGE":
                newMsg = new ImageMessage(balasChatService.balasImage(), balasChatService.balasImage());
                break;
            case "TEST PUSH":
                newMsg = new TextMessage(balasChatService.balasPush());
            default:
                newMsg = new TextMessage("Command tidak ditemukan! \nGET ID, SAY HELLO, SEND IMAGE, TEST PUSH");
                break;
        }

        ReplyMessage replyMessage = new ReplyMessage(replyToken, newMsg);

        // up to 5 messages at once
        // replyMessage = new ReplyMessage(replyToken, new LinkedList<Message>());

        try {
            lineMessagingClient
                    .replyMessage(replyMessage)
                    .get();
            System.out.println("Success sending message");
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (pesan.equalsIgnoreCase("TEST PUSH")) {
            System.out.println("Push message");
            handlePushEvent(userId, "Message push.");
        } else {
            System.out.println("No push message");
        }

    }


    public void handlePushEvent(String userID, String message) {
        List<Message> messageList = new LinkedList<Message>();

        TextMessage textMessage = new TextMessage(message);
        ImageMessage imageMessage = new ImageMessage(URI.create("https://i.imgur.com/GujvJ2S.jpg"), URI.create("https://i.imgur.com/GujvJ2S.jpg"));
        messageList.add(textMessage);
        messageList.add(imageMessage);

        try {
            lineMessagingClient
                    .pushMessage(new PushMessage(userID, messageList))
                    .get();
        } catch (NullPointerException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
